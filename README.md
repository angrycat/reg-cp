Набор для разработки
===

Установка
---

Для работы необходимо, чтобы были установлены [nodejs](http://nodejs.org), [bower](http://bower.io), глобально установленный [gulpjs](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md).

***

Клонируйте проект в локальную директорию (далее `REG-CP`). Далее в терминале:

```
bower install
npm install
```

После установки всех необходимых пакетов можно находясь в REG-CP использовать команду `gulp`.

- Компиляция SCSS файлов из `dev/scss` в `root/assets/main.css`
- Конкатенация JS файлов из `dev/js` в `root/assets/main.js`
- Компиляция HTML файлов
- Минимизация изображений
- Файл index.html будет открыт в браузере Google chrome
- Все изменения вносимые в файлах находящихся в директории `dev` и её поддиректориях будут отслеживаться. При необходимости будут запускаться задачи по обработке соответствующих файлов и изменения будут загружены в браузер без перезагрузки.
