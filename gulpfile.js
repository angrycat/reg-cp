var gulp = require('gulp'),
    bs = require('browser-sync').create(),
    reload = bs.reload,
    fi = require('gulp-file-include'),
    gulpif = require('gulp-if'),
    plumber = require('gulp-plumber'),
    gutil = require('gulp-util'),
    notify = require('gulp-notify'),
    sass = require('gulp-sass'),
    sequence = require('gulp-sequence'),
    sourcemap = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    changed = require('gulp-changed'),
    del = require('del'),
    autoprefixer = require('gulp-autoprefixer'),
    cfg = require('./config.json'),
    debug = true;


// Error Handle
var onError = function(error) {
    console.log(error.message);
    this.emit('end');
};

// TASKS

// Init autoreload
gulp.task('serve', ['build'], function (cb) {
    bs.init({
        open: 'local',
        browser: 'google chrome',
        server: {
            baseDir: cfg.dest.root,
            index: 'index.html'
        }
    });
});

// Clear destinations
//  See more here: http://stackoverflow.com/questions/24396659/how-to-clean-a-project-correctly-with-gulp
gulp.task('clean', function(cb) {
    debug = false;
    return del(cfg.dest.clean, {cwd: cfg.dest.root}).then(function(paths){
        console.log('Files and folders that would be deleted:\n', paths.join('\n'));
    });
});

// Styles
gulp.task('styles', function(cb) {
    return gulp.src(cfg.src.styles, {cwd: cfg.src.root})
        .pipe(gulpif(debug, sourcemap.init()))
        .pipe(plumber({errorHandler: onError}))
        .pipe(sass({
            sourceComments: debug,
            outputStyle: debug ? 'nested' : 'compressed'
        }))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(gulpif(
            debug,
            sourcemap.write('.')
        ))
        .pipe(plumber.stop())
        .pipe(gulp.dest(cfg.dest.styles, {
            cwd: cfg.dest.root
        }))
        .pipe(reload({stream: true}));
});

// Images
gulp.task('images', function(cb) {
    return gulp.src(cfg.src.images, {cwd: cfg.src.root})
        .pipe(changed(cfg.dest.images, {cwd: cfg.dest.root}))
        .pipe(gulpif(!debug, imagemin({
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            use: [pngquant()],
            interlaced: true
        })))
        .pipe(plumber({errorHandler: onError}))
        .pipe(gulp.dest(cfg.dest.images, {
            cwd: cfg.dest.root
        }))
        .pipe(reload({stream: true}));
});

// Scripts
gulp.task('scripts', function(cb) {
    return gulp.src(cfg.src.js, {cwd: cfg.src.root})
        .pipe(gulpif(
            debug,
            sourcemap.init()
        ))
        .pipe(plumber({errorHandler: onError}))
        .pipe(fi({prefix: '///'}))
        .pipe(plumber({errorHandler: onError}))
        .pipe(gulpif(
            !debug,
            uglify()
        ))
        .pipe(gulpif(
            debug,
            sourcemap.write('.')
        ))
        .pipe(plumber.stop())
        .pipe(gulp.dest(cfg.dest.js, {cwd: cfg.dest.root}))
        .pipe(reload({stream: true}));
});

// html
gulp.task('html', function(cb) {
    return gulp.src(cfg.src.html, {cwd: cfg.src.root})
        .pipe(plumber({errorHandler: onError}))
        .pipe(fi({prefix: '@@'}))
        .pipe(plumber({errorHandler: onError}))
        .pipe(changed(cfg.watch.html, {cwd: cfg.dest.root}))
        .pipe(plumber.stop())
        .pipe(gulp.dest(cfg.dest.html, {cwd: cfg.dest.root}))
        .pipe(reload({stream: true}));
});

// copy
gulp.task('copy', function(cb) {
    return gulp.src(cfg.src.copy)
        .pipe(changed(cfg.dest.copy, {cwd: cfg.dest.root}))
        .pipe(gulp.dest(cfg.dest.copy, {cwd: cfg.dest.root}))
        .pipe(reload({stream: true}));
});

// Build
gulp.task('build', [
    'html',
	'styles',
	'scripts',
	'copy',
	'images'
]);


gulp.task('production', sequence('clean', 'build'));

// Watch
gulp.task('watch', ['build'], function () {
    gulp.watch(cfg.watch.html, {
        cwd: cfg.src.root
    }, ['html']);
    gulp.watch(cfg.watch.styles, {
        cwd: cfg.src.root
    }, ['styles']);
    gulp.watch(cfg.watch.js, {
        cwd: cfg.src.root
    }, ['scripts']);
    gulp.watch(cfg.watch.images, {
        cwd: cfg.src.root
    }, ['images']);
    gulp.watch(cfg.src.copy, {
        cwd: cfg.src.root
    }, ['copy']);
});


gulp.task('default', ['build', 'serve', 'watch']);
