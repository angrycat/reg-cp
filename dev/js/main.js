// Main.js

///include('../../bower/jquery/dist/jquery.js')
///include('../../bower/tooltipster/dist/js/tooltipster.bundle.js')
///include('../../bower/magnific-popup/dist/jquery.magnific-popup.js')
///include('modules/data-password.js')
///include('modules/data-expandable.js')
///include('modules/jquery.accordion.js')
///include('modules/data-toggle.js')
///include('modules/data-tabs.js')

$(document).ready(function() {
    $('[data-tooltip]').tooltipster();
    $('[data-accordion]').accordion();
    $('[data-dialog]').magnificPopup({
        type:            'inline',
        fixedContentPos: false,
        fixedBgPos:      true,
        overflowY:       'auto',
        removalDelay:    300,
        mainClass:       'mfp--zoom-in',
        preloader:       false,
        modal:           true
    });
    $(document).on('click', '[data-dialog-cancel], [data-dialog-confirm]', function(e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
});
