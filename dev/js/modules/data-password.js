// Show/Hide password
(function() {
    'use strict';

    $('[data-password]').each(function() {
        var $this = $(this),
            $target = $($this.data('password')),
            $icon = null;

        if ($target.length) {
            $icon = $('<i/>').addClass('data-password data-password--show').appendTo($this);
            console.log($icon);
            $this.on('click', function(e) {
                e.preventDefault();
                if ($target.attr('type') == 'password') {
                    $target.attr('type', 'text');
                    $icon.removeClass('data-password--show').addClass('data-password--hide');
                } else {
                    $target.attr('type', 'password');
                    $icon.removeClass('data-password--hide').addClass('data-password--show');
                }
            });
        }
    });
})();
