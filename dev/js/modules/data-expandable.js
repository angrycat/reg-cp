(function() {
    'use strict';

    $('[data-expandable]').each(function() {
        var $this = $(this),
            $main = $this.children('li'),
            $sub = $('ul', $main);

        $('ul', $main.not('.expanded')).hide();

        $main.on('click', function(e) {
            var $this = $(this),
                $show = $('ul', $this);
            e.preventDefault();

            if ($show.hasClass('expanded') === false) {
                $main.removeClass('expanded');
                $sub.slideUp();
                $this.addClass('expanded');
                $show.slideDown();
            }
        });
    });
})();
