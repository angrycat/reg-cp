(function() {
    'use strict';
    var $body = $('body');

    $('[data-toggle]').each(function() {
        var $this = $(this),
            $target = $($this.data('toggle'));

        if ($target.length) {
            $this.on('click', function(e) {
                e.preventDefault();
                $this.toggleClass('toggled');
                $target.toggleClass('toggled');
            });
        }
    });
})();
