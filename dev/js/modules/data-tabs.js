(function() {
    'use strict';

    // FIX: Придётся переписать, потому что вложенные табы влияют на внешние

    var tabs = $('[data-action *= "tabs"]');

    tabs.each(function() {
        var $tab = $(this),
        $tabsItems = $tab.find('[data-tab-target]'),
        $tabContent = $tab.find('[data-tab-content]'),
        $tabContentWrapper = $tabContent.eq(0).parent();

        $tabsItems.on('click', function(e) {
            event.preventDefault();
			var $selectedItem = $(this);

            if( $selectedItem.hasClass('selected') === false ) {
                var selectedTab = $selectedItem.data('tab-target'),
                    $selectedContent = $tab.find('[data-tab-content="'+selectedTab+'"]'),
                    selectedContentHeight = $selectedContent.innerHeight();

                $tabsItems.removeClass('selected');
                $selectedItem.addClass('selected');
                $tabContent.removeClass('selected');
                $selectedContent.addClass('selected');

                console.log(selectedContentHeight);

                $tabContentWrapper.animate({
                    'height': selectedContentHeight
                }, 400);
            }
        });
    });

    $(window).on('resize', function() {
        tabs.each(function() {
            var $tab = $(this);
            $tab.find('[data-tab-content]').css('height', 'auto');
        });
    });
})();
